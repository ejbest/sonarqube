import requests
import json,sys
# print(sys.argv[1])
s = requests.Session()
r = s.get('%s/crumbIssuer/api/json'%(sys.argv[1]), auth=('%s'%(sys.argv[2]), '%s'%(sys.argv[3])))
crumb_user=json.loads(r.text)
r = s.post('%s/asynchPeople/api/json'%(sys.argv[1]),auth=('%s'%(sys.argv[2]), '%s'%(sys.argv[3])),data={'Jenkins-Crumb':crumb_user['crumb'],'Content-Type':'text/xml'},)
user_json=r.text
print (user_json)
json_user=json.loads(user_json)
user_list=json_user['users']
for i in user_list:
    print('User Name:  %s'%(i['user']['fullName']))
    print('Last Change:  %s'%(i['lastChange']))
    print('Project:   %s '%(i['project']))
    print("======================================")    

s.close()