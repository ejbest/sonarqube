#---------------------------------------------------------------------------
# Name:        Jenkins Stats
# Purpose:
#
# Created:     19/09/2019
#
# Required : pip install jenkinsapi
#            pip install selenium
#            Download https://chromedriver.storage.googleapis.com/index.html?path=78.0.3904.11/

#---------------------------------------------------------------------------
import sys
import jenkinsapi.jenkins as jenkins
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

'''    
   imports for selenium
'''
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

def printNumberOfJobs(server):
    jobs = server.get_jobs()
    counter = 0
    for job_name, job_instance in server.get_jobs():
        counter += 1
    print("Number Of Jobs : ".ljust(18), counter)

def printNumberOfNodes(server):
    nodes = server.get_nodes()
    print("Number Of Nodes : ".ljust(18), len(nodes))

def printNumberOfUsers(server):
    driver = webdriver.Chrome(".")
    driver.get(server.url)
    
    elem = driver.find_element_by_name("j_username")
    elem.send_keys(server.username)
    elem.send_keys(Keys.RETURN)
    
    elem = driver.find_element_by_name("j_password")
    elem.send_keys(server.password)
    elem.send_keys(Keys.RETURN)
    driver.implicitly_wait(15)
    
    driver.get(server.url + "/personAsynch")
    driver.implicitly_wait(5)
    print(driver.find_elements_by_tag_name('tr'))
    driver.close()
    
    
    
def cleanAdminFolder(server):
    pass

def loginWithCredentials():
    if (len(sys.argv) == 1):
        username = "admin"
        password = "6f380778a4d0488f92635d870a5a80ed"
        url = "http://127.0.0.1:8080"
    else:
        username = sys.argv[2]
        password = sys.argv[3]
        url = sys.argv[1]

    server = jenkins.Jenkins(url, username=username, password=password,ssl_verify=False)
    version = server.version

    server.username = username
    server.password = password
    server.url = url
    return server


def main():
    server = loginWithCredentials()
    printNumberOfJobs(server)
    printNumberOfNodes(server)
    printNumberOfUsers(server)
    cleanAdminFolder(server)

if __name__ == '__main__':
    main()
