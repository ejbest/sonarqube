#-------------------------------------------------------------------------------
# Name:        Jenkins Stats
# Purpose:
#
# Required : pip install jenkinsapi
#            pip install bs4

#-------------------------------------------------------------------------------

import sys
import jenkinsapi.jenkins as jenkins

'''
    imports for webscraping
'''
import requests
import urllib.request
from bs4 import BeautifulSoup

def printNumberOfJobs(server):
    jobs = server.get_jobs()
    counter = 0
    for job_name, job_instance in server.get_jobs():
        counter += 1
    print("Number Of Jobs : ", counter)

def printNumberOfNodes(server):
    nodes = server.get_nodes()
    print("Number Of Nodes : ", len(nodes))

def printNumberOfUsers(server):
    print(server)
    loginurl = server.url + "/j_acegi_security_check?j_username=" +server.username+ "&j_password="+server.password + "&from=/"
    print(loginurl)
    loginHead = { 'User-Agent' : 'Mozilla/5'}
    loginResponse = requests.post(loginurl)
    ##print(loginResponse.headers, loginResponse.cookies)
    cookie = ""
    for c in loginResponse.cookies:
        cookie = cookie + c.name + "=" + c.value + ";"
    ##print(cookie)
    url = server.url + "/securityRealm/"
    headers = {'Cookie': cookie}
    response = requests.get(url,headers=headers)


    soup = BeautifulSoup(response.text, "html.parser")
    print ("Number Of Users : ", len(soup.findAll('tr')) )

def cleanAdminFolder(server):
    pass

def loginWithCredentials():
    if (len(sys.argv) == 1):
        username = "admin"
        password = "6f380778a4d0488f92635d870a5a80ed"
        url = "http://localhost:8080"
    else:
        username = sys.argv[2]
        password = sys.argv[3]
        url = sys.argv[1]

    server = jenkins.Jenkins(url, username=username, password=password,ssl_verify=False)
    version = server.version
    print('Hello %s from Jenkins %s' % (username, version))
    server.username = username
    server.password = password
    server.url = url
    return server


def main():
    server = loginWithCredentials()
    printNumberOfJobs(server)
    printNumberOfNodes(server)
    printNumberOfUsers(server)
    cleanAdminFolder(server)

if __name__ == '__main__':
    main()
