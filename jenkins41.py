#---------------------------------------------------------------------------
# Name:        Jenkins Stats
# Purpose:
#
# Created:     19/09/2019
#
# Required : pip install jenkinsapi
#            pip install selenium
#            Download phantomjs and copy executable to same folder

#---------------------------------------------------------------------------
import sys
import jenkinsapi.jenkins as jenkins
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import requests
import json

'''
   imports for selenium
'''
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import warnings

def printNumberOfJobs(server):
    jobs = server.get_jobs()
    counter = 0
    for job_name, job_instance in server.get_jobs():
        counter += 1
    print("Number Of Jobs : ".ljust(18), counter)

def printNumberOfNodes(server):
    nodes = server.get_nodes()
    print("Number Of Nodes : ".ljust(18), len(nodes))

def printNumberOfUsers(server):
    warnings.filterwarnings("ignore")
    page = requests.get(
        server.url + "/asynchPeople/api/json?pretty=true",
        auth=(server.username , server.password),
        verify=False,
    ).content
    page = page.decode("utf8").replace("'", '"')
    users = json.loads(page)
    users = users["users"]
    count = 0
    for user in users:
        user = user["user"]
        count += 1
        print(user["fullName"])
    
    print("Number of Users".ljust(18), count)

def cleanAdminFolder(server):
    pass

def loginWithCredentials():
    if (len(sys.argv) == 1):
        username = "admin"
        password = "6f380778a4d0488f92635d870a5a80ed"
        url = "http://127.0.0.1:8080"
    else:
        username = sys.argv[2]
        password = sys.argv[3]
        url = sys.argv[1]

    server = jenkins.Jenkins(url, username=username, password=password,ssl_verify=False)

    server.username = username
    server.password = password
    server.url = url
    return server


def main():
    server = loginWithCredentials()
    printNumberOfUsers(server)
    printNumberOfJobs(server)
    printNumberOfNodes(server)
    cleanAdminFolder(server)

if __name__ == '__main__':
    main()
