#-------------------------------------------------------------------------------
# Name:        Jenkins Stats
# Purpose:
#
# Author:      martin.ferreira
#
# Created:     19/09/2019
# Copyright:   (c) martin.ferreira 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import sys
import jenkins

def printNumberOfJobs(server):
    jobs = server.get_jobs()
    print("Number Of Jobs : ", len(jobs))

def printNumberOfNodes(server):
    nodes = server.get_nodes()
    print("Number Of Nodes : ", len(nodes))

def printNumberOfUsers(server):
    pass

def cleanAdminFolder(server):
    pass

def loginWithCredentials():
    if (len(sys.argv) == 1):
        username = "admin"
        password = "6f380778a4d0488f92635d870a5a80ed"
        url = "http://localhost:8080"
    else:
        username = sys.argv[1]
        password = sys.argv[2]
        url = sys.argv[3]

    server = jenkins.Jenkins(url, username=username, password=password)
    user = server.get_whoami()
    version = server.get_version()
    print('Hello %s from Jenkins %s' % (user['fullName'], version))
    return server


def main():
    server = loginWithCredentials()
    printNumberOfJobs(server)
    printNumberOfNodes(server)
    #printNumberOfUsers(server)
    cleanAdminFolder(server)

if __name__ == '__main__':
    main()
