# Get all users from Jenkins

 ## Installation
The  program is created in a virtual environment using pipenv. If you do not want to use the virtual environment install the following modules using pip3:
-  requests
- click

To install pipenv use:
> $ pip3 install pipenv

To install the virtual environment use ()in the folder with the Pipfile:
> $ pipenv install

## Usage
To run the program execute:
> $ pipenv shell
> $ python get_users.py domain_link username password

If the domain, username or password are missing an error with the text:
> Error: Missing argument "PASSWORD"

will appear.
If one of them is incorrect the error:
>Error: Invalid input. Could not get users!

will appear.

Example of command:
> python get_users.py http://localhost:8080/ root
