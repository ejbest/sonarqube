import requests
import click
import json

CREDENTIALS = {"AUTH": {"username": "", "password": ""}, "DOMAIN": ""}


def get_page():
    page = requests.get(
        f"{CREDENTIALS['DOMAIN']}/asynchPeople/api/json?pretty=true",
        auth=(CREDENTIALS["AUTH"]["username"], CREDENTIALS["AUTH"]["password"]),
    ).content

    page = page.decode("utf8").replace("'", '"')
    return json.loads(page)


def get_users():
    users = get_page()
    users = users["users"]

    count = 0
    for user in users:
        user = user["user"]
        count += 1
        print(user["fullName"])

    print(f"You have a total of {count} users at the domain:{CREDENTIALS['DOMAIN']}")


@click.command()
@click.argument("domain")
@click.argument("username")
@click.argument("password")
def main(domain, username, password):
    global CREDENTIALS
    CREDENTIALS = {
        "AUTH": {"username": username, "password": password},
        "DOMAIN": domain,
    }

    try:
        get_users()
    except:
        print("Error: Invalid input. Could not get users!")


if __name__ == "__main__":
    main()
