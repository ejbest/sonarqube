import requests
import click
import json
import logging

CREDENTIALS = {"AUTH": {"username": "", "password": ""}, "DOMAIN": ""}

logger = logging.getLogger(__name__)


def configure_logging():
    """configure logging module"""
    logging.basicConfig(
        # filename=CONFIG["LOGGING_FILE"],
        level=logging.INFO,
        format="%(asctime)s: %(levelname)7s: [%(name)s]: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )


def get_page():
    logger.debug(f"Sending get request to the domain: {CREDENTIALS['DOMAIN']}")

    page = requests.get(
        f"{CREDENTIALS['DOMAIN']}/asynchPeople/api/json?pretty=true",
        auth=(CREDENTIALS["AUTH"]["username"], CREDENTIALS["AUTH"]["password"]),
    ).content

    logger.debug(f"Succesfully got page of users from domain: {CREDENTIALS['DOMAIN']}")
    page = page.decode("utf8").replace("'", '"')
    return json.loads(page)


def get_users():
    users = get_page()
    users = users["users"]

    logger.debug(f"Starting to get users!")
    count = 0
    for user in users:
        user = user["user"]
        count += 1
        print(user["fullName"])
    logger.debug(f"Succsesfully get users!")

    print(f"You have a total of {count} users at the domain:{CREDENTIALS['DOMAIN']}")


@click.command()
@click.argument("domain")
@click.argument("username")
@click.argument("password")
def main(domain, username, password):
    logger.info(f"Program started!")
    global CREDENTIALS
    CREDENTIALS = {
        "AUTH": {"username": username, "password": password},
        "DOMAIN": domain,
    }

    try:
        get_users()

    except Exception as e:
        logger.warning(f"Error: Could not get users. Error message:{e}!")

    else:
        logger.info(f"Programfinish succesffuly!")


if __name__ == "__main__":
    configure_logging()
    main()
