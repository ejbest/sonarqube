import sys
from pprint import pprint

import requests

if len(sys.argv) < 4:
    print("The format should be `sonar_stas.py url username password")
else:
    pprint(sys.argv)

    host = sys.argv[1]
    user = sys.argv[2]
    password = sys.argv[3]

    # init session
    session = requests.Session()
    session.auth = user, password
    endpoint = "%s/api/projects/search" % host

    print("Endpoint is %s" % endpoint)

    res = requests.get(url=endpoint)

    pprint(res.text)




